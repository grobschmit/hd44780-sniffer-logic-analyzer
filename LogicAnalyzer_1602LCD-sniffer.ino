/*
Robert Sturzbecher 2017
Test board to sniff/extract data sent to HD44780 based 16x2LCD screens running in 4bit mode

A Atmega328 at 16Mhz seems too slow for sniffing the data from my target device so does not get all the data. 
Have tried making the code as quick as possible and offloading the decoding to a second device but still not capturing all data.
but have a few more tricks to try like 
  1) Clocking the Arduino at 20Mhz or overclocking to 25-30Mhz
  2) Not sniffing the data but instead dumping it's ram after it has been updated
  3) Using a faster processor


Pins
Arduino LCD 
A0-A3   D4-d7
A4      WS
A5      EN
*/

#include <avr/interrupt.h>

//quick and easy ring buffer, limited to 256 bytes but want it to be as simple as possible and with as little clockcycles as possible.
volatile uint8_t bufferdata[256];                            //buffer of data, going to try this to remove the serial.write from the ISR, did not seem to improve speed
volatile uint8_t bufferindex_read;                           //uint8 as we want to wrap back to 0 after 256 (buffer size), index of last byte read
volatile uint8_t bufferindex_write;                          //uint8 as we want to wrap back to 0 after 256 (buffer size), index of last byte written 


void setup() {
    Serial.begin(115200);
    Serial.println("\nBooting");
    cli();                                                   //disable irq while we are changing them
    DDRC = B00000000;                                        //set all of PORTC (A0-A5) as input
    PCICR |= B00000010;                                      //Enables Pin Change Interrupts on portD
    //PCMSK1 |= B00100000;                                   //A5(EN), triggers only on EN change
    PCMSK1 |= B00111111;                                     //A0-A5, trigger if any of the pins change, allows code to work with other purposes in mind
    sei();                                                   //enable irq 
}



void loop() {
    if (bufferindex_read != bufferindex_write){              //data has been added to buffer 
        bufferindex_read++;                                 
        Serial.write(bufferdata[bufferindex_read]);          //send data from buffer to serial port, as raw. use this if using something connected to serial port to decode the data
        //Serial.println(bufferdata[bufferindex_read],BIN);  //send data from buffer to serial port as binary, slower
    }
}



ISR(PCINT1_vect){
     //volatile byte bits = PINC & B00111111;                        //read the low 6 bits of PORTC (A0-A5)
     //Serial.write(bits);
     bufferdata[bufferindex_write++] = PINC & B00111111;             //read the low 6 bits of PORTC (A0-A5)
}