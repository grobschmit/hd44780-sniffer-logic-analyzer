# README #

Code to test if possible to use an Arduino to sniff data sent to a HD44780 16x2LCD running in 4 bit mode. 

Tried to keep the code as lean as possible and offload as much of the decoding to a seconds processor but still was not fast enough to keep up with the data. 

Posting the code as could be helpful for others for direct IO port reading (multiple pins) or basic circular data buffering. Might still work for HD44780 sniffing if the target is using a slower data transfer rate or if a faster clock speed is used on the Arduino. The code can also be used as a basic 6bit logic analyzer.

The code was designed for a atmega328 based Arduino but may work with other atmel based boards.